INSERT INTO `munchies`.role (`role`) VALUES ('ADMIN');
INSERT INTO `munchies`.role (`role`) VALUES ('USER');

INSERT INTO munchies.user (id, firstname, lastname, phone, email, password, address)
 VALUES
(1, 'admin', 'admin', '11111111', 'admin@admin.admin', '$2a$12$ISC5u1US39hLepi0y9b3h.hKJivrhYnR7ff94edmgaMkgUklqJAoO', 'Tamo negde1');
-- pass Admin1

INSERT INTO `user_role` (`user_id`, `role_id`) VALUES (1, 1);