

CREATE TABLE `User` (
	`id` bigint NOT NULL AUTO_INCREMENT,
	`firstname` varchar(25),
	`lastname` varchar(25),
	`phone` varchar(30),
	`email` varchar(30) NOT NULL UNIQUE,
	`password` varchar(30) NOT NULL,
	`address` varchar(50),
	PRIMARY KEY (`id`)
);

CREATE TABLE `Role` (
	`id` bigint NOT NULL AUTO_INCREMENT,
	`role` varchar(25) NOT NULL UNIQUE,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Group_Order` (
	`id` bigint NOT NULL AUTO_INCREMENT,
	`timeout` varchar(25) NOT NULL DEFAULT '10',
	`restaurant_id` bigint NOT NULL,
	`creator` varchar(25) NOT NULL,
	`order_url` varchar(250) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Restaurant` (
	`id` bigint NOT NULL AUTO_INCREMENT,
	`name` varchar(30) NOT NULL UNIQUE,
	`address` varchar(30),
	`phone` varchar(30) NOT NULL UNIQUE,
	`menu_url` varchar(250) NOT NULL,
	`delivery_info` varchar(250),
	PRIMARY KEY (`id`)
);

CREATE TABLE `User_Role` (
	`role_id` bigint NOT NULL,
	`user_id` bigint NOT NULL,
	primary  key (`role_id`,`user_id`)
);

CREATE TABLE `Item` (
	`id` bigint NOT NULL AUTO_INCREMENT,
	`group_order_id` bigint NOT NULL,
	`item_name` varchar(25) NOT NULL,
	`creator` varchar(25) NOT NULL,
	`price` double NOT NULL,
	PRIMARY KEY (`id`)
);

ALTER TABLE `Group_Order` ADD CONSTRAINT `Group_Order_fk0` FOREIGN KEY (`restaurant_id`) REFERENCES `Restaurant`(`id`);

ALTER TABLE `User_Role` ADD CONSTRAINT `User_Role_fk0` FOREIGN KEY (`role_id`) REFERENCES `Role`(`id`);

ALTER TABLE `User_Role` ADD CONSTRAINT `User_Role_fk1` FOREIGN KEY (`user_id`) REFERENCES `User`(`id`);

ALTER TABLE `Item` ADD CONSTRAINT `Item_fk0` FOREIGN KEY (`group_order_id`) REFERENCES `Group_Order`(`id`);

