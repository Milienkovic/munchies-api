package com.munchies.api.controllers.exceptionHandlers;

import com.munchies.api.models.payload.ErrorResponse;
import com.munchies.api.services.exceptions.InvalidJwtAuthenticationException;
import io.jsonwebtoken.JwtException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ExceptionHandlerExceptionResolver;

import java.sql.Timestamp;

@ControllerAdvice
public class JwtErrorsHandler extends ExceptionHandlerExceptionResolver {

    @ExceptionHandler(value = {JwtException.class, IllegalArgumentException.class, InvalidJwtAuthenticationException.class})
    public ResponseEntity<ErrorResponse> invalidToken(RuntimeException e){
        ErrorResponse response= new ErrorResponse();
        response.setMessage(e.getMessage());
        response.setStatus(HttpStatus.NETWORK_AUTHENTICATION_REQUIRED.value());
        response.setTimestamp( new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.status(HttpStatus.NETWORK_AUTHENTICATION_REQUIRED).body(response);
    }
}
