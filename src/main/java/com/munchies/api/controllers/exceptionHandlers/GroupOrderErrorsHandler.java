package com.munchies.api.controllers.exceptionHandlers;

import com.munchies.api.models.payload.ErrorResponse;
import com.munchies.api.services.exceptions.GroupOrderNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.sql.Timestamp;

@ControllerAdvice
public class GroupOrderErrorsHandler {

    @ExceptionHandler(GroupOrderNotFoundException.class)
    public ResponseEntity<ErrorResponse> notFound(GroupOrderNotFoundException e){
        ErrorResponse response = new ErrorResponse();
        response.setMessage(e.getMessage());
        response.setStatus(HttpStatus.NOT_FOUND.value());
        response.setTimestamp(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    }
}
