package com.munchies.api.controllers.exceptionHandlers;

import com.munchies.api.models.payload.ErrorResponse;
import com.munchies.api.services.exceptions.StorageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.io.IOException;
import java.sql.Timestamp;

@ControllerAdvice
public class StorageExceptionHandler {

    @ExceptionHandler(value = {StorageException.class})
    public ResponseEntity<ErrorResponse> storageNotFound(StorageException e){
        ErrorResponse response = new ErrorResponse();
        response.setMessage(e.getMessage());
        response.setStatus(HttpStatus.NOT_FOUND.value());
        response.setTimestamp(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    }

    @ExceptionHandler(value = {IOException.class})
    public ResponseEntity<ErrorResponse> IOExceptionHandler(IOException e){
        ErrorResponse response = new ErrorResponse();
        response.setMessage(e.getMessage());
        response.setStatus(HttpStatus.NOT_FOUND.value());
        response.setTimestamp(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    }
}
