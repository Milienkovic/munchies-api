package com.munchies.api.controllers.exceptionHandlers;

import com.munchies.api.models.payload.ErrorResponse;
import com.munchies.api.services.exceptions.RestaurantExistsException;
import com.munchies.api.services.exceptions.RestaurantNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.sql.Timestamp;

@ControllerAdvice
public class RestaurantErrorsHandler {

    @ExceptionHandler(value = {RestaurantNotFoundException.class, RestaurantExistsException.class})
    public ResponseEntity<ErrorResponse> restaurantNotFound(RuntimeException e) {
        ErrorResponse response = new ErrorResponse();
        response.setMessage(e.getMessage());
        response.setTimestamp(new Timestamp(System.currentTimeMillis()));
        int status=418;
        if (e instanceof RestaurantExistsException) {
            status = HttpStatus.BAD_REQUEST.value();
        }else if (e instanceof RestaurantNotFoundException){
            status = HttpStatus.NOT_FOUND.value();
        }
        response.setStatus(status);
        return ResponseEntity.status(status).body(response);
    }


}
