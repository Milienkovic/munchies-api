package com.munchies.api.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import com.munchies.api.models.payload.RestaurantDTO;
import com.munchies.api.services.data.RestaurantService;
import com.munchies.api.services.data.StorageService;
import com.munchies.api.services.exceptions.RestaurantExistsException;
import com.munchies.api.services.markers.Views;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
@Slf4j
public class RestaurantController {

    private static final String RESTAURANT_URL = "/api/v1/restaurants";

    private final RestaurantService restaurantService;
    private final StorageService storageService;

    public RestaurantController(RestaurantService restaurantService, StorageService storageService) {
        this.restaurantService = restaurantService;
        this.storageService = storageService;
    }

    /**
     * Find all restaurants
     */
    @JsonView(Views.Public.class)
    @GetMapping(value = "/restaurants", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<RestaurantDTO>> getRestaurants() {
        if (restaurantService.findAll().isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return ResponseEntity.ok().body(restaurantService.findAll());
    }

    /**
     * Find restaurant by ID
     */
    @JsonView(Views.Internal.class)
    @GetMapping(value = "/restaurants/{restaurantId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RestaurantDTO> findById(@PathVariable("restaurantId") Integer id) {
        RestaurantDTO restaurant = restaurantService.findById(id);

        return ResponseEntity.ok().body(restaurant);
    }

    /**
     * Create restaurant json
     */
    @PreAuthorize("hasAuthority('ADMIN')")
    @JsonView(Views.Public.class)
    @PostMapping(value = "/restaurants", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RestaurantDTO> createRestaurant(@Valid @RequestBody RestaurantDTO restaurantDTO,
                                                          HttpServletRequest request){
        RestaurantDTO rest = restaurantService.saveOrUpdate(restaurantDTO);

        return ResponseEntity.created(ServletUriComponentsBuilder
                .fromContextPath(request)
                .path(RESTAURANT_URL + "/{id}")
                .buildAndExpand(rest.getId())
                .toUri()).body(rest);
    }

    /**
     * Create restaurant from form
     */
    @PreAuthorize("hasAuthority('ADMIN')")
    @JsonView(Views.Public.class)
    @PostMapping(value = "/restaurants", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<RestaurantDTO> createRestaurant(
            @Valid @ModelAttribute RestaurantDTO restaurantDTO,
            @RequestPart("file") MultipartFile file, HttpServletRequest request) throws Exception {

        RestaurantDTO rest = restaurantService.saveOrUpdate(restaurantDTO, file);
        return ResponseEntity.created(ServletUriComponentsBuilder
                .fromContextPath(request)
                .path("/api/v1/restaurants/{id}")
                .buildAndExpand(rest.getId())
                .toUri()).body(rest);
    }


    /**
     * Upload restaurant's menu file
     */
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/restaurants/{restaurantId}/menu", method = {RequestMethod.POST,RequestMethod.PUT},
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<String> uploadMenu(@PathVariable Integer restaurantId,
                                             @RequestPart MultipartFile file,
                                             HttpServletRequest request) throws Exception {
        RestaurantDTO restaurant = restaurantService.findById(restaurantId);

        if (!file.isEmpty()) {
            restaurantService.saveOrUpdate(restaurant, file);
            return ResponseEntity.created(ServletUriComponentsBuilder
                    .fromContextPath(request)
                    .path("api/v1/restaurants/{restaurantId}/download")
                    .buildAndExpand(restaurantId)
                    .toUri())
                    .body("File: " + file.getOriginalFilename() + " has been uploaded");
        } else {
            return ResponseEntity.badRequest().body("Empty file");
        }
    }

    /**
     * Delete restaurant
     */
    @PreAuthorize("hasAuthority('ADMIN')")
    @DeleteMapping("/restaurants/{restaurantId}")
    public ResponseEntity<Void> deleteRestaurant(@PathVariable("restaurantId") Integer id) {
        restaurantService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    /**
     * Edit restaurant
     */
    @PreAuthorize("hasAuthority('ADMIN')")
    @JsonView(Views.Internal.class)
    @PutMapping("/restaurants/{restaurantId}")
    public ResponseEntity<RestaurantDTO> updateRestaurant(
            @Valid @RequestBody RestaurantDTO restaurantDTO,
            @PathVariable("restaurantId") Integer id) throws RestaurantExistsException {

        RestaurantDTO restaurant = restaurantService.saveOrUpdate(restaurantDTO,id);
        return ResponseEntity.ok().body(restaurant);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @JsonView(Views.Internal.class)
    @PatchMapping("/restaurants/{restaurantId}")
    public ResponseEntity<RestaurantDTO> partialUpdateRestaurant(
            @Valid @RequestBody RestaurantDTO restaurantDTO,
            @PathVariable("restaurantId") Integer id) {

        RestaurantDTO restaurant = restaurantService.saveOrUpdate(restaurantDTO,id);
        return ResponseEntity.ok().body(restaurant);
    }

    @GetMapping("/restaurants/{restaurantId}/download")
    public ResponseEntity<Resource> downloadMenu(@PathVariable Integer restaurantId) throws IOException {
        RestaurantDTO restaurantDTO = restaurantService.findById(restaurantId);
        HttpHeaders headers = new HttpHeaders();
        String contentDispositionValue = "attachment; fileName=";
        headers.add(HttpHeaders.CONTENT_DISPOSITION, contentDispositionValue + restaurantDTO.getMenuUrl());
        Resource resource = storageService.loadAsResource(restaurantDTO.getMenuUrl());

        return ResponseEntity.ok()
                .headers(headers)
                .contentLength(resource.getFile().length())
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(resource);
    }
}




