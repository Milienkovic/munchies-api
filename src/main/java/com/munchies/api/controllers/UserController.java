package com.munchies.api.controllers;

import com.munchies.api.models.payload.UserDTO;
import com.munchies.api.services.data.UserService;
import com.munchies.api.services.security.jwt.LoggedInUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/v1/api")
public class UserController {

    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("/users")
    public ResponseEntity<List<UserDTO>> getUsers() {
        List<UserDTO> users = userService.findAll();
        if (users.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(users);

    }

    @PreAuthorize("isAuthenticated() and (hasAuthority('ADMIN') or @userServiceImpl.isCurrentUser(#userId,#principal.name))")
    @GetMapping("/users/{userId}")
    public ResponseEntity<UserDTO> findById(@PathVariable Integer userId, Principal principal) {

            return ResponseEntity.ok(userService.findById(userId));
    }

    @PostMapping("/users")
    public ResponseEntity<?> createUser(@Valid @RequestBody UserDTO user) {
        UserDTO userDTO = userService.saveOrUpdate(user);
        return ResponseEntity.created(ServletUriComponentsBuilder
                .fromCurrentRequestUri()
                .path("/{userId}")
                .buildAndExpand(userDTO.getId())
                .toUri()
        ).body(userDTO);
    }

    @PreAuthorize("isAuthenticated() and @userServiceImpl.isCurrentUser(#userId,#principal.name)")
    @PutMapping("/users/{userId}")
    public ResponseEntity<?> updateUser(@Valid @RequestBody UserDTO userDTO, @PathVariable Integer userId, Principal principal) {

            UserDTO user = userService.saveOrUpdate(userDTO, userId);
            return ResponseEntity.ok(user);
    }

    @PreAuthorize("isAuthenticated() and @userServiceImpl.isCurrentUser(#userId,#principal.name)")
    @DeleteMapping("/users/{userId}")
    public ResponseEntity<?> deleteUser(@PathVariable Integer userId, Principal principal) {
            userService.deleteById(userId);
            return ResponseEntity.noContent().build();
    }


    @PreAuthorize("isAuthenticated()")
    @GetMapping("/me")
    public ResponseEntity currentUser(@LoggedInUser UserDetails userDetails, HttpServletRequest request) {
        return ResponseEntity.ok(userService.getCurrentUser(userDetails, request.getUserPrincipal()));
    }
}
