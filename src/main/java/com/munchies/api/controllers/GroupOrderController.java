package com.munchies.api.controllers;


import com.fasterxml.jackson.annotation.JsonView;
import com.munchies.api.models.payload.GroupOrderDTO;
import com.munchies.api.services.data.GroupOrderService;
import com.munchies.api.services.data.RestaurantService;
import com.munchies.api.services.markers.Views;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class GroupOrderController {

    private final GroupOrderService groupOrderService;

    public GroupOrderController(GroupOrderService groupOrderService, RestaurantService restaurantService) {
        this.groupOrderService = groupOrderService;
    }
    @GetMapping({"/orders"})
    @JsonView(Views.Public.class)
    public ResponseEntity<List<GroupOrderDTO>> getGroupOrders() {
        List<GroupOrderDTO> orders = groupOrderService.findAll();
        if (orders.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(orders);
    }

    @GetMapping("/orders/{orderId}")
    @JsonView(Views.Internal.class)
    public ResponseEntity<GroupOrderDTO> findOrder(@PathVariable("orderId") Integer id) {
        GroupOrderDTO groupOrderDTO = groupOrderService.findById(id);
        return ResponseEntity.ok(groupOrderDTO);
    }
    @GetMapping("/restaurants/{restaurantId}/orders")
    @JsonView(Views.Public.class)
    public ResponseEntity<List<GroupOrderDTO>> findOrders(@PathVariable("restaurantId") Integer id) {
        List<GroupOrderDTO> orders = groupOrderService.findAllByRestaurantId(id);
        if (orders.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(orders);
    }

    @GetMapping("/restaurants/{restaurantId}/active")
    @JsonView(Views.Public.class)
    public ResponseEntity<List<GroupOrderDTO>> getActiveOrders(@PathVariable Integer restaurantId) {
        List<GroupOrderDTO> activeOrders = groupOrderService.activeOrders(restaurantId);
        if (activeOrders.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(activeOrders);
    }

    @PostMapping("/restaurants/{restaurantId}/orders")
    @JsonView(Views.Public.class)
    public ResponseEntity<GroupOrderDTO> createGroupOrder(
            @Valid @RequestBody GroupOrderDTO groupOrderDTO,
            @PathVariable Integer restaurantId,
            HttpServletRequest request){

        GroupOrderDTO order = groupOrderService.saveOrUpdate(groupOrderDTO, restaurantId);
        return ResponseEntity.created(ServletUriComponentsBuilder
        .fromContextPath(request)
        .path("/api/v1/restaurants/{restaurantId}/orders/{orderId}")
        .buildAndExpand(restaurantId,order.getId())
        .toUri()).body(order);
    }

    @PutMapping("/restaurants/{restaurantId}/orders/{orderId}")
    @JsonView(Views.Public.class)
    public ResponseEntity<GroupOrderDTO> editGroupOrder(
            @Valid @RequestBody GroupOrderDTO groupOrderDTO, @PathVariable Integer orderId,
            @PathVariable Integer restaurantId){

        GroupOrderDTO order = groupOrderService.saveOrUpdate(groupOrderDTO, restaurantId,orderId);
        return ResponseEntity.ok(order);
    }

    @DeleteMapping("/orders/{orderId}")
    public ResponseEntity<Void> deleteOrder(@PathVariable("orderId") Integer id) {
        groupOrderService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
