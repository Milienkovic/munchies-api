package com.munchies.api.controllers;


import com.fasterxml.jackson.annotation.JsonView;
import com.munchies.api.models.payload.ItemDTO;
import com.munchies.api.services.data.ItemService;
import com.munchies.api.services.markers.Views;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class ItemController {

    private final ItemService itemService;

    public ItemController(ItemService itemService) {
        this.itemService = itemService;
    }

    @JsonView(Views.Public.class)
    @GetMapping({"/items"})
    public ResponseEntity<List<ItemDTO>> getAllItems() {
        List<ItemDTO> items = itemService.findAll();
        if (items.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(itemService.findAll());
    }

    @JsonView(Views.Internal.class)
    @GetMapping("/items/{itemId}")
    public ResponseEntity<ItemDTO> getItem(@PathVariable Integer itemId) {
        ItemDTO item = itemService.findById(itemId);
        return ResponseEntity.ok().body(item);
    }

    @JsonView(Views.Public.class)
    @GetMapping("/orders/{orderId}/items")
    public ResponseEntity<List<ItemDTO>> getItemsFromOrder(@PathVariable Integer orderId) {
        List<ItemDTO> items = itemService.findAllByOrderId(orderId);
        if (items.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(items);
    }

    @JsonView(Views.Public.class)
    @PostMapping("/orders/{orderId}/items")
    public ResponseEntity<ItemDTO> createItem(@Valid @RequestBody ItemDTO itemDTO,
                                              @PathVariable Integer orderId,
                                              HttpServletRequest request) {

        ItemDTO item = itemService.saveOrUpdate(itemDTO, orderId);
        return ResponseEntity.created(ServletUriComponentsBuilder
                .fromContextPath(request)
                .path("/api/v1/orders/{orderId}/items/{itemId}")
                .buildAndExpand(orderId,item.getId())
                .toUri()
        ).body(item);
    }

    @JsonView(Views.Internal.class)
    @PutMapping("/orders/{orderId}/items/{itemId}")
    public ResponseEntity<ItemDTO> editItem(@Valid @RequestBody ItemDTO itemDTO,
                                            @PathVariable Integer itemId,
                                            @PathVariable Integer orderId){

        ItemDTO returnItem = itemService.saveOrUpdate(itemDTO, orderId, itemId);
        return ResponseEntity.ok(returnItem);
    }

    @DeleteMapping("/items/{itemId}")
    public ResponseEntity<Void> deleteItem(@PathVariable Integer itemId) {
        itemService.deleteById(itemId);
        return ResponseEntity.noContent().build();
    }
}
