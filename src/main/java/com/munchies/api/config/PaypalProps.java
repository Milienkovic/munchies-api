package com.munchies.api.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "paypal")
public class PaypalProps {
    private String clientId;
    private String clientSecret;
    private String mode;

}
