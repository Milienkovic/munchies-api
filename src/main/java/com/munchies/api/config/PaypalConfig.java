package com.munchies.api.config;

import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.OAuthTokenCredential;
import com.paypal.base.rest.PayPalRESTException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class PaypalConfig {

    private PaypalProps paypalProps;

    public PaypalConfig(PaypalProps paypalProps) {
        this.paypalProps = paypalProps;
    }

    @Bean
    public Map<String,String> paypalSdkConfig(){
        Map<String,String> config = new HashMap<>();
        config.put("mode",paypalProps.getMode());
        return config;
    }

    @Bean
    public OAuthTokenCredential authTokenCredential(){
        return  new OAuthTokenCredential(paypalProps.getClientId(),paypalProps.getClientSecret(),paypalSdkConfig());
    }

    @Bean
    public APIContext context() throws PayPalRESTException {
        APIContext context = new APIContext(authTokenCredential().getAccessToken());
        context.setMode(paypalProps.getMode());
        return context;
    }

}
