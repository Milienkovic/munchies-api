package com.munchies.api.services.data;


import com.munchies.api.models.payload.ItemDTO;

import java.util.List;

public interface ItemService {
    List<ItemDTO> findAll();
    List<ItemDTO> findAllByOrderId(Integer id);
    ItemDTO findById(Integer id);
    ItemDTO saveOrUpdate(ItemDTO item, Integer orderId);
    ItemDTO saveOrUpdate(ItemDTO itemDTO, Integer orderId, Integer itemId);
    void deleteById(Integer id);

    Double totalSum(Integer orderId);
}
