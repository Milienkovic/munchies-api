package com.munchies.api.services.data;

import com.munchies.api.models.payload.RestaurantDTO;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;
import java.util.stream.Stream;

public interface StorageService {


    void init();

    void store(MultipartFile multipartFile, RestaurantDTO restaurantDTO) throws IOException;

    Stream<Path> loadAll();

    Path load(String filename);

    Resource loadAsResource(String filename);

    void deleteAll();

    void delete(String filename);

    void write(MultipartFile file, String fileName) throws IOException;

    void download(OutputStream stream, String fileName);
}

