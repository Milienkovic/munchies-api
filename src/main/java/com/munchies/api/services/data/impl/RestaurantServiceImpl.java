package com.munchies.api.services.data.impl;


import com.munchies.api.models.payload.RestaurantDTO;
import com.munchies.api.models.entities.Restaurant;
import com.munchies.api.repo.RestaurantRepo;
import com.munchies.api.services.data.RestaurantService;
import com.munchies.api.services.data.StorageService;
import com.munchies.api.services.exceptions.RestaurantExistsException;
import com.munchies.api.services.exceptions.RestaurantNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class RestaurantServiceImpl implements RestaurantService {

    private RestaurantRepo repo;

    private StorageService storageService;

    private ModelMapper modelMapper;

    public RestaurantServiceImpl(RestaurantRepo repo, StorageService storageService, ModelMapper modelMapper) {
        this.repo = repo;
        this.storageService = storageService;
        this.modelMapper = modelMapper;
    }

    @Override
    @Transactional
    public List<RestaurantDTO> findAll() {
        return repo.findAll().stream()
                .map(restaurant -> modelMapper.map(restaurant, RestaurantDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public RestaurantDTO findById(Integer id) {
        Restaurant restaurant = repo.findById(id).orElseThrow(() -> new RestaurantNotFoundException("Restaurant with ID=" + id + " doesn'PaypalPaymentIntent exist"));
        return modelMapper.map(restaurant, RestaurantDTO.class);
    }

    @Override
    public RestaurantDTO saveOrUpdate(RestaurantDTO restaurantDTO) {
        return saveRestaurant(restaurantDTO, restaurantDTO.getId());
    }

    @Override
    public RestaurantDTO saveOrUpdate(RestaurantDTO restaurantDTO, Integer restaurantId) {
        restaurantDTO.setId(restaurantId);
        return saveRestaurant(restaurantDTO, restaurantId);
    }


    private RestaurantDTO saveRestaurant(RestaurantDTO restaurant, Integer restaurantId) {
        Restaurant returnRestaurant;
        checkRestaurantsUniqueness(restaurant, restaurantId);

        if (restaurant.getId() == 0) {
            returnRestaurant = repo.save(modelMapper.map(restaurant, Restaurant.class));
        } else {
            RestaurantDTO dbRestaurant = findById(restaurantId);
            modelMapper.map(restaurant, dbRestaurant);
            returnRestaurant = repo.save(modelMapper.map(dbRestaurant, Restaurant.class));
        }
        return modelMapper.map(returnRestaurant, RestaurantDTO.class);
    }

    @Override
    public RestaurantDTO saveOrUpdate(RestaurantDTO restaurantDTO, MultipartFile file) throws IOException {

        setMenu(restaurantDTO, file);
        return saveRestaurant(restaurantDTO, restaurantDTO.getId());
    }

    private void setMenu(RestaurantDTO restaurant, MultipartFile file) throws IOException {

        if (restaurant.getId() != 0) {
            RestaurantDTO dbRestaurant = findById(restaurant.getId());
            if (file.isEmpty() && dbRestaurant.getMenuUrl() != null) {
                restaurant.setMenuUrl(dbRestaurant.getMenuUrl());
            }
            if (!file.isEmpty() && restaurant.getMenuUrl() != null && dbRestaurant.getMenuUrl() != null) {
                String extension = FilenameUtils.getExtension(file.getOriginalFilename());
                String oldExtension = dbRestaurant.getMenuUrl().substring(dbRestaurant.getMenuUrl().lastIndexOf("."));
                if (!extension.equals(oldExtension)) {
                    storageService.delete(dbRestaurant.getMenuUrl());
                }
            }
        }
        if (!file.isEmpty()) {
            restaurant.setMenuUrl(restaurant.getName() + "_menu." + FilenameUtils.getExtension(file.getOriginalFilename()));
            storageService.store(file, restaurant);
        }
    }

    @Override
    public void deleteById(Integer id) {
        if (!repo.existsById(id)) {
            throw new RestaurantNotFoundException("Restaurant with ID=" + id + " doesn'PaypalPaymentIntent exist");
        }
        repo.deleteById(id);
    }

    @Override
    @Transactional
    public RestaurantDTO findByName(String name) {
        Restaurant restaurant = repo.findByName(name)
                .orElseThrow(() -> new RestaurantNotFoundException("Restaurant with NAME=" + name + " doesn'PaypalPaymentIntent exist"));
        return modelMapper.map(restaurant, RestaurantDTO.class);
    }

    @Override
    public RestaurantDTO findByPhone(String phone) {
        Restaurant restaurant = repo.findByEmail(phone)
                .orElseThrow(() -> new RestaurantNotFoundException("Restaurant with PHONE=" + phone + " doesn'PaypalPaymentIntent exist"));
        return modelMapper.map(restaurant, RestaurantDTO.class);
    }


    @Override
    @Transactional
    public RestaurantDTO findByEmail(String email) {
        Restaurant restaurant = repo.findByEmail(email)
                .orElseThrow(() -> new RestaurantNotFoundException("Restaurant with EMAIL=" + email + " doesn'PaypalPaymentIntent exist"));
        return modelMapper.map(restaurant, RestaurantDTO.class);
    }

    private void checkRestaurantsUniqueness(RestaurantDTO restaurantDTO, Integer id) {
        if (id != null) {
            restaurantDTO.setId(id);
        }

        repo.findByEmail(restaurantDTO.getEmail()).ifPresent(restaurant -> {
            if (restaurant.getId() != restaurantDTO.getId()) {
                throw new RestaurantExistsException("There is already a restaurant registered with that email");
            }
        });

        repo.findByName(restaurantDTO.getName()).ifPresent(restaurant -> {
            if (restaurant.getId() != restaurantDTO.getId()) {
                throw new RestaurantExistsException("There is already a restaurant registered with that name");
            }
        });

        repo.findByPhone(restaurantDTO.getPhone()).ifPresent(restaurant -> {
            if (restaurant.getId() != restaurantDTO.getId()) {
                throw new RestaurantExistsException("There is already a restaurant registered with that phone");
            }
        });
    }

    private boolean isUnique(RestaurantDTO restaurantDTO, Integer id) {
        if (id != null) {
            restaurantDTO.setId(id);
        }
        return repo.findAll().stream()
                .noneMatch(restaurant -> restaurant.getId() != restaurantDTO.getId() &&
                        (restaurant.getName().equals(restaurantDTO.getName()) ||
                                restaurant.getPhone().equals(restaurantDTO.getPhone()) ||
                                restaurant.getEmail().equals(restaurantDTO.getEmail())));
    }
}
