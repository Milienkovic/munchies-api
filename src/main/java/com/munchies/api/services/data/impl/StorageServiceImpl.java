package com.munchies.api.services.data.impl;

import com.munchies.api.models.payload.RestaurantDTO;
import com.munchies.api.services.data.StorageService;
import com.munchies.api.services.exceptions.StorageException;
import com.munchies.api.services.exceptions.StorageFileNotFoundException;
import com.munchies.api.util.StorageProperties;
import org.apache.commons.io.FilenameUtils;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.stream.Stream;

@Service
public class StorageServiceImpl implements StorageService {

    private final Path rootLocation;

    public StorageServiceImpl(StorageProperties properties) {
        this.rootLocation = Paths.get(properties.getLocation());
    }

    @Override
    public void init() {
        try {
            if (!Files.exists(rootLocation)) {
                Files.createDirectories(rootLocation);
            }

        } catch (IOException e) {
            throw new StorageException("could not initialize storage", e);
        }
    }

    @Override
    public void store(MultipartFile file, RestaurantDTO restaurantDTO) throws IOException {
        String filename = StringUtils.cleanPath(file.getOriginalFilename());
        String extension = FilenameUtils.getExtension(filename);
        if (file.isEmpty()) {
            throw new StorageException("failed to store empty file" + filename);
        }
        if (filename.contains("..")) {
            throw new StorageException("Cannot store file with relative path outside current directory " + filename);
        }

        filename = restaurantDTO.getName() + "_menu." + extension;
        InputStream inputStream = file.getInputStream();
        Files.copy(inputStream,load(filename), StandardCopyOption.REPLACE_EXISTING);
    }

    @Override
    public Stream<Path> loadAll() {
        try {
            return Files.walk(this.rootLocation, 1).filter(path -> !path.equals(this.rootLocation)).map(this.rootLocation::relativize);
        } catch (IOException e) {
            throw new StorageException("failed to read stored files", e);
        }
    }

    @Override
    public Path load(String filename) {
        return rootLocation.resolve(filename);
    }

    @Override
    public Resource loadAsResource(String filename) {
        try {
            Path file = load(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() && resource.isReadable()) {
                return resource;
            } else {
                throw new StorageFileNotFoundException("could not read file:" + filename);
            }
        } catch (MalformedURLException e) {
            throw new StorageFileNotFoundException("could not read file:" + filename, e);
        }

    }

    @Override
    public void deleteAll() {
        FileSystemUtils.deleteRecursively(rootLocation.toFile());
    }

    @Override
    public void delete(String filename) {
        File file = load(filename).toFile();
        file.delete();
    }

    @Override
    public void write(MultipartFile file,String fileName) throws IOException {
        Path path = load(fileName);
        byte[] bytes;
        if (!file.isEmpty()) {
            bytes = file.getBytes();
            Files.write(path, bytes);
        }
    }

    @Override
    public void download(OutputStream stream, String fileName) {
        Path file = load(fileName);
        if (Files.exists(file)) {
            try {
                Files.copy(file, stream);
                stream.flush();
                stream.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

}
