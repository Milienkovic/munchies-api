package com.munchies.api.services.data.impl;


import com.munchies.api.models.payload.GroupOrderDTO;
import com.munchies.api.models.payload.RestaurantDTO;
import com.munchies.api.models.entities.GroupOrder;
import com.munchies.api.repo.GroupOrderRepo;
import com.munchies.api.services.data.GroupOrderService;
import com.munchies.api.services.data.RestaurantService;
import com.munchies.api.services.exceptions.GroupOrderNotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GroupOrderServiceImpl implements GroupOrderService {

    private GroupOrderRepo repo;
    private RestaurantService restaurantService;
    private ModelMapper groupOrderMapper;

    public GroupOrderServiceImpl(GroupOrderRepo repo, RestaurantService restaurantService, ModelMapper groupOrderMapper) {
        this.repo = repo;
        this.restaurantService = restaurantService;
        this.groupOrderMapper = groupOrderMapper;
    }

    @Override
    @Transactional
    public List<GroupOrderDTO> findAll() {
        return repo.findAll().stream()
                .map(groupOrder -> groupOrderMapper.map(groupOrder, GroupOrderDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<GroupOrderDTO> findAllByRestaurantId(Integer id) {
        return repo.findAllByRestaurantId(id).stream()
                .map(groupOrder -> groupOrderMapper.map(groupOrder, GroupOrderDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public GroupOrderDTO findById(Integer id) {
        GroupOrder groupOrder = repo.findById(id).orElseThrow(() -> new GroupOrderNotFoundException("Order with ID=" + id + " doesn'PaypalPaymentIntent exist"));
        return groupOrderMapper.map(groupOrder, GroupOrderDTO.class);
    }

    @Override
    public GroupOrderDTO saveOrUpdate(GroupOrderDTO groupOrderDTO, Integer restaurantId) {
        return saveOrder(groupOrderDTO,restaurantId);
    }

    @Override
    public GroupOrderDTO saveOrUpdate(GroupOrderDTO groupOrderDTO, Integer restaurantId, Integer id) {
        groupOrderDTO.setId(id);
        return saveOrder(groupOrderDTO,restaurantId);
    }

    @Override
    public GroupOrderDTO saveOrUpdate(GroupOrderDTO groupOrderDTO) {
        return saveOrder(groupOrderDTO,groupOrderDTO.getRestaurant().getId());
    }

    @Override
    public void deleteById(Integer id) {
        if (!repo.existsById(id)) {
            throw new GroupOrderNotFoundException("Order with ID=" + id + " doesn'PaypalPaymentIntent exist");
        }
        repo.deleteById(id);
    }

    @Override
    @Transactional
    public List<GroupOrderDTO> activeOrders(Integer restaurantId) {
        Calendar timeoutExpire = Calendar.getInstance();
        Calendar currentTime = Calendar.getInstance();
        currentTime.getTime();
        List<GroupOrderDTO> orders = repo.findAllByRestaurantId(restaurantId).stream()
                .map(groupOrder -> groupOrderMapper.map(groupOrder, GroupOrderDTO.class))
                .collect(Collectors.toList());

        return orders.stream()
                .peek(groupOrderDTO -> {
                    timeoutExpire.setTime(groupOrderDTO.getTimestamp());
                    timeoutExpire.add(Calendar.MINUTE, Integer.valueOf(groupOrderDTO.getTimeout()));
                }).filter(groupOrderDTO -> timeoutExpire.after(currentTime))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public Long timer(Integer orderId) {
        GroupOrderDTO order = findById(orderId);
        Calendar timeoutExpire = Calendar.getInstance();
        Calendar currentTime = Calendar.getInstance();
        currentTime.getTime();
        timeoutExpire.setTime(order.getTimestamp());
        timeoutExpire.add(Calendar.MINUTE, Integer.parseInt(order.getTimeout()));
        return timeoutExpire.getTimeInMillis() - currentTime.getTimeInMillis();
    }

    private GroupOrderDTO saveOrder(GroupOrderDTO order, Integer restaurantId){
        GroupOrder savedOrder;
        RestaurantDTO restaurant = restaurantService.findById(restaurantId);
        order.setRestaurant(restaurant);

        if (order.getId()==0){
            savedOrder=repo.save(groupOrderMapper.map(order,GroupOrder.class));
        }else {
            GroupOrderDTO dbOrder = findById(order.getId());
            groupOrderMapper.map(order,dbOrder);
            savedOrder = repo.save(groupOrderMapper.map(dbOrder,GroupOrder.class));
        }
        return groupOrderMapper.map(savedOrder,GroupOrderDTO.class);
    }
}
