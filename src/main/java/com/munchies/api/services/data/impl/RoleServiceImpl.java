package com.munchies.api.services.data.impl;

import com.munchies.api.models.entities.Role;
import com.munchies.api.repo.RoleRepo;
import com.munchies.api.services.data.RoleService;
import com.munchies.api.services.exceptions.RoleNotFoundException;
import org.springframework.stereotype.Service;


@Service
public class RoleServiceImpl implements RoleService {

    private RoleRepo roleRepo;

    public RoleServiceImpl(RoleRepo roleRepo) {
        this.roleRepo = roleRepo;
    }

    @Override
    public Role findByRole(String type) {
        return roleRepo.findByRole(type).orElseThrow(() -> new RoleNotFoundException("Role not found"));
    }

    @Override
    public Role findById(Integer id) {
       return roleRepo.findById(id).orElseThrow(() -> new RoleNotFoundException("Role not found"));
    }
}
