package com.munchies.api.services.data.impl;

import com.munchies.api.models.payload.GroupOrderDTO;
import com.munchies.api.models.payload.ItemDTO;
import com.munchies.api.models.entities.Item;
import com.munchies.api.repo.ItemRepo;
import com.munchies.api.services.data.GroupOrderService;
import com.munchies.api.services.data.ItemService;
import com.munchies.api.services.exceptions.ItemNotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ItemServiceImpl implements ItemService {

    private ItemRepo repo;

    private ModelMapper itemMapper;

    private GroupOrderService groupOrderService;

    public ItemServiceImpl(ItemRepo repo, ModelMapper itemMapper, GroupOrderService groupOrderService) {
        this.repo = repo;
        this.itemMapper = itemMapper;
        this.groupOrderService = groupOrderService;
    }

    @Override
    public List<ItemDTO> findAll() {
        return repo.findAll().stream()
                .map(item -> itemMapper.map(item, ItemDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<ItemDTO> findAllByOrderId(Integer id) {
        return repo.findAllByGroupOrderId(id).stream()
                .map(item -> itemMapper.map(item, ItemDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public ItemDTO findById(Integer id) {
        Item item = repo.findById(id).orElseThrow(() -> new ItemNotFoundException("Item with ID=" + id + " doesn'PaypalPaymentIntent exist"));

        if (item == null) {
            return null;
        }
        return itemMapper.map(item, ItemDTO.class);
    }

    @Override
    public ItemDTO saveOrUpdate(ItemDTO item, Integer orderId) {
        return saveItem(item, orderId);
    }

    @Override
    public ItemDTO saveOrUpdate(ItemDTO item, Integer orderId, Integer itemId) {
        item.setId(itemId);
        return saveItem(item, orderId);
    }

    @Override
    public void deleteById(Integer id) {
        if (!repo.existsById(id)) {
            throw new ItemNotFoundException("Item with ID=" + id + " doesn'PaypalPaymentIntent exist");
        }
        repo.deleteById(id);
    }

    @Override
    public Double totalSum(Integer orderId) {
        return repo.findAllByGroupOrderId(orderId).stream()
                .map(item -> item.getPrice() * item.getQuantity())
                .reduce(0.0, Double::sum);
    }

    private ItemDTO saveItem(ItemDTO item, Integer orderId) {
        Item returnItem;
        GroupOrderDTO order = groupOrderService.findById(orderId);
        item.setGroupOrder(order);

        if (item.getId() == 0) {
            returnItem = repo.save(itemMapper.map(item, Item.class));
        } else {
            ItemDTO dbItem = findById(item.getId());
            itemMapper.map(item, dbItem);
            returnItem = repo.save(itemMapper.map(dbItem, Item.class));
        }
        return itemMapper.map(returnItem, ItemDTO.class);
    }
}
