package com.munchies.api.services.data;


import com.munchies.api.models.entities.Role;

public interface RoleService {

    Role findByRole(String role);

    Role findById(Integer id);
}
