package com.munchies.api.services.data;

import com.munchies.api.models.payload.RestaurantDTO;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface RestaurantService {
    List<RestaurantDTO> findAll();
    RestaurantDTO findById(Integer id);
    RestaurantDTO saveOrUpdate(RestaurantDTO restaurant);
    RestaurantDTO saveOrUpdate(RestaurantDTO restaurantDTO, Integer restaurantId);
    RestaurantDTO saveOrUpdate(RestaurantDTO restaurantDTO, MultipartFile file) throws IOException;
    void deleteById(Integer id);
    RestaurantDTO findByName(String name);
    RestaurantDTO findByPhone(String phone);
    RestaurantDTO findByEmail(String email);
}
