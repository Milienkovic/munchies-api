package com.munchies.api.services.data;


import com.munchies.api.models.payload.GroupOrderDTO;

import java.util.List;

public interface GroupOrderService {
    List<GroupOrderDTO> findAll();
    List<GroupOrderDTO> findAllByRestaurantId(Integer id);
    GroupOrderDTO findById(Integer id);
    GroupOrderDTO saveOrUpdate(GroupOrderDTO groupOrder, Integer restaurantId, Integer orderId);
    GroupOrderDTO saveOrUpdate(GroupOrderDTO groupOrderDTO, Integer restaurantId);
    GroupOrderDTO saveOrUpdate(GroupOrderDTO groupOrderDTO);
    void deleteById(Integer id);
    List<GroupOrderDTO> activeOrders(Integer restaurantId);
    Long timer(Integer orderId);



}
