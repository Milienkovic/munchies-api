package com.munchies.api.services.data.impl;


import com.munchies.api.models.entities.Role;
import com.munchies.api.models.entities.User;
import com.munchies.api.models.payload.UserDTO;
import com.munchies.api.repo.RoleRepo;
import com.munchies.api.repo.UserRepo;
import com.munchies.api.services.data.UserService;
import com.munchies.api.services.exceptions.UserNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@Service
@Slf4j
public class UserServiceImpl implements UserService {


    private RoleRepo roleRepo;
    private PasswordEncoder passwordEncoder;
    private UserRepo userRepo;
    private ModelMapper userMapper;

    public UserServiceImpl(RoleRepo roleRepo, PasswordEncoder passwordEncoder, UserRepo userRepo, ModelMapper userMapper) {
        this.roleRepo = roleRepo;
        this.passwordEncoder = passwordEncoder;
        this.userRepo = userRepo;
        this.userMapper = userMapper;
    }

    @Override
    public List<UserDTO> findAll() {
        return userRepo.findAll().stream()
                .map(user -> userMapper.map(user, UserDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public UserDTO findById(Integer id) {
        User user = userRepo.findById(id).orElseThrow(() -> new UserNotFoundException("User with id:" + id + " doesn't exist"));
        return userMapper.map(user, UserDTO.class);
    }

    @Override
    public UserDTO saveOrUpdate(UserDTO user) {
        return saveUser(user, user.getId());
    }

    @Override
    public void deleteById(Integer id) {
        if (!userRepo.existsById(id)) {
            throw new UserNotFoundException("User with id:" + id + " doesn't exist");
        }
        userRepo.deleteById(id);
    }

    @Override
    public UserDTO findByEmail(String email) {
        User user = userRepo.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException("User not found"));
        return userMapper.map(user, UserDTO.class);
    }

    @Override
    public Map<Object, Object> getCurrentUser(UserDetails userDetails, Principal principal) {
        Map<Object, Object> model = new HashMap<>();
        model.put("username", principal.getName());
        model.put("roles", userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(toList()));
        return model;
    }

    @Override
    public UserDTO saveOrUpdate(UserDTO userDTO, Integer userId) {
        userDTO.setId(userId);
        return saveUser(userDTO, userId);
    }

    private UserDTO saveUser(UserDTO user, Integer userId) {
        User returnUser;
        if (user.getId() == 0) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));

            Set<Role> roles = new HashSet<>();
            Role role = roleRepo.findByRole("USER").orElse(null);
            roles.add(role);
            user.setUserRoles(roles);

            returnUser = userRepo.save(userMapper.map(user, User.class));
        } else {
            UserDTO dbUser = findById(userId);
            userMapper.map(user, dbUser);
            returnUser = userRepo.save(userMapper.map(dbUser, User.class));
        }
        return userMapper.map(returnUser, UserDTO.class);
    }

    @Override
    public boolean isAuthorizedUser(Integer userId, String username) {
        return isCurrentUser(userId, username) || isAdmin(username);
    }

    private boolean isAdmin(String username) {
        UserDTO user = findByEmail(username);
        return user.getUserRoles().stream()
                .anyMatch(role -> role.getRole().equals("ADMIN"));
    }

    @Override
    public boolean isCurrentUser(Integer userId, String username) {
        UserDTO currentUser = findByEmail(username);
        UserDTO requestedUser = findById(userId);
        log.error("Users " + currentUser.equals(requestedUser));
        return currentUser.equals(requestedUser);
    }


}
