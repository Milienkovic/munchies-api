package com.munchies.api.services.data;



import com.munchies.api.models.payload.UserDTO;
import org.springframework.security.core.userdetails.UserDetails;

import java.security.Principal;
import java.util.List;
import java.util.Map;

public interface UserService {
    List<UserDTO> findAll();
    UserDTO findById(Integer id);
    UserDTO saveOrUpdate(UserDTO user);
    void deleteById(Integer id);
    UserDTO findByEmail(String email);

    Map<Object, Object> getCurrentUser(UserDetails userDetails, Principal principal);

    UserDTO saveOrUpdate(UserDTO userDTO, Integer userId);

    boolean isAuthorizedUser(Integer userId, String username);

    boolean isCurrentUser(Integer userId, String username);
}
