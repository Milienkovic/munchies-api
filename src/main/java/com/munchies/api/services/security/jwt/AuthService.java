package com.munchies.api.services.security.jwt;

import com.munchies.api.models.entities.Role;
import com.munchies.api.models.payload.AuthenticationRequest;
import com.munchies.api.services.data.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Slf4j
public class AuthService {

    private UserService userService;

    private AuthenticationManager authenticationManager;

    private JwtTokenProvider jwtTokenProvider;

    public AuthService(UserService userService, AuthenticationManager authenticationManager, JwtTokenProvider jwtTokenProvider) {
        this.userService = userService;
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    public Map<String, String> signIn(AuthenticationRequest data) {
        String username = data.getUsername();
        Set<Role> roleSet = userService.findByEmail(username).getUserRoles();
        Set<String> roles = roleSet.stream().map(role -> role.getRole()).collect(Collectors.toSet());
        Authentication authentication = new UsernamePasswordAuthenticationToken(username, data.getPassword());
        log.error("auth " + authentication);
        Authentication res = authenticationManager.authenticate(authentication);
        log.error("after " + res);
        String token = jwtTokenProvider.createToken(username, roles);

        Map<String, String> model = new HashMap<>();
        model.put("username", username);
        model.put("token", token);

        return model;
    }
}
