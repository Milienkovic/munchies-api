package com.munchies.api.services.validators.validators;

import com.munchies.api.services.validators.Password;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordValidator implements ConstraintValidator<Password, String> {

    public void initialize(Password constraint) {
    }

    public boolean isValid(String password, ConstraintValidatorContext context) {
        if (password != null) {
            return password.length() > 3 && password.length() < 9
                    && password.matches("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{4,8}$");
        }
        return true;
    }
}
