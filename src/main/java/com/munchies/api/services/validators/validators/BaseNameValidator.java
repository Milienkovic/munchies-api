package com.munchies.api.services.validators.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.annotation.Annotation;

public abstract class BaseNameValidator<T extends Annotation> implements ConstraintValidator<T,String> {
    String regex;
    @Override
    public void initialize(T constraint) {
        try {
            regex = constraint.annotationType().getMethod("regexp").getDefaultValue().toString();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return s.length() > 2 && s.length() <= 20 && Character.isUpperCase(s.charAt(0))
                && s.matches(regex);
    }
}
