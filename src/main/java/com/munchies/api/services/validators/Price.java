package com.munchies.api.services.validators;

import com.munchies.api.services.validators.validators.PriceValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PriceValidator.class)
@Target({ElementType.FIELD, ElementType.PARAMETER})
public @interface Price {
    String message() default "Invalid price format";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
