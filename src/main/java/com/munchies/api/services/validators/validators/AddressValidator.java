package com.munchies.api.services.validators.validators;

import com.munchies.api.services.validators.Address;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class AddressValidator implements ConstraintValidator<Address, String> {
   String regex;
   public void initialize(Address constraint) {
      regex = constraint.regexp();
   }

   public boolean isValid(String address, ConstraintValidatorContext context) {
      return Character.isUpperCase(address.charAt(0)) && address.length()>=2
              && address.length()<=50 && address.matches(regex);
   }
}
