package com.munchies.api.services.validators.validators;

import com.munchies.api.services.validators.Email;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CustomEmailValidator implements ConstraintValidator<Email, String> {
    String regex;
    public void initialize(Email constraint) {
         regex = constraint.regexp();
    }

    public boolean isValid(String email, ConstraintValidatorContext context) {
            return email.matches(regex);

    }
}
