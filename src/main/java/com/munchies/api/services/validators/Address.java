package com.munchies.api.services.validators;

import com.munchies.api.services.validators.validators.AddressValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Target({ElementType.FIELD,ElementType.METHOD,ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = AddressValidator.class)
public @interface Address {
    String message() default "Invalid address, first character must be uppercase";
    Class<?>[] groups() default {};
    Class<? extends Payload> [] payload() default {};
    String regexp() default "^[A-Z][A-Za-z0-9:\\- *]{3,50}$";
}
