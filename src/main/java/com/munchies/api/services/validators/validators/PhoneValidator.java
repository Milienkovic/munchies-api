package com.munchies.api.services.validators.validators;

import com.munchies.api.services.validators.Phone;
import org.springframework.beans.factory.annotation.Value;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Valid;

public class PhoneValidator implements ConstraintValidator<Phone, String> {

   public void initialize(Phone constraint) {
   }

   public boolean isValid(String phone, ConstraintValidatorContext context) {
      return phone.matches("^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\\s\\./0-9]*$");
   }
}
