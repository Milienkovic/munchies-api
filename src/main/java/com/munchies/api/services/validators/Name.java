package com.munchies.api.services.validators;

import com.munchies.api.services.validators.validators.NameValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = NameValidator.class)
@Target({ElementType.FIELD,ElementType.METHOD,ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface Name {
    String message() default "Invalid name, first character must be uppercase, only letters and numbers allowed";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
    String regexp() default "^[A-Z][A-Za-z0-9 ]{3,20}$";
}
