package com.munchies.api.services.validators;

import com.munchies.api.services.validators.validators.PasswordValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD,ElementType.METHOD,ElementType.PARAMETER})
@Constraint(validatedBy = PasswordValidator.class)
public @interface Password {
    String message() default "Invalid Password, must contain at least 1 uppercase Character and a Digit";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
    String regexp() default ".*";
}
