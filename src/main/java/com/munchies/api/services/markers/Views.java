package com.munchies.api.services.markers;
public final class Views {

    public interface Public{}

    public interface Internal extends Public{}
}
