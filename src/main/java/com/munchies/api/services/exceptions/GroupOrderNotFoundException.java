package com.munchies.api.services.exceptions;

public class GroupOrderNotFoundException extends RuntimeException {
    public GroupOrderNotFoundException() {
    }

    public GroupOrderNotFoundException(String message) {
        super(message);
    }

    public GroupOrderNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
