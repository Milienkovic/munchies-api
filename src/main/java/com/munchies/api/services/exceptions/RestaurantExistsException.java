package com.munchies.api.services.exceptions;

public class RestaurantExistsException extends RuntimeException {
    public RestaurantExistsException(String message) {
        super(message);
    }
}
