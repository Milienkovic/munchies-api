package com.munchies.api.services.exceptions;

public class MailAlreadySentException extends RuntimeException{
    public MailAlreadySentException() {
    }

    public MailAlreadySentException(String message) {
        super(message);
    }

    public MailAlreadySentException(String message, Throwable cause) {
        super(message, cause);
    }
}
