package com.munchies.api.services.mail;

import com.munchies.api.services.mail.task.MyTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Component
public class EmailScheduler {

    private MyTask myTask;

    public EmailScheduler(MyTask myTask) {
        this.myTask = myTask;
    }

    ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
    TaskScheduler scheduler = new ConcurrentTaskScheduler(executorService);

    @Scheduled(fixedDelay = 30000)
    public void sendOrders() {
        scheduler.schedule(myTask, new Date());
    }
}
