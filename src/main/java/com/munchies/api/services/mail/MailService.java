package com.munchies.api.services.mail;

import com.munchies.api.models.payload.GroupOrderDTO;
import com.munchies.api.services.data.GroupOrderService;
import com.munchies.api.services.data.ItemService;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.internet.InternetAddress;

@Service
public class MailService {

    private JavaMailSender sendMail;

    private GroupOrderService groupOrderService;

    private ItemService itemService;

    public MailService(JavaMailSender sendMail, GroupOrderService groupOrderService, ItemService itemService) {
        this.sendMail = sendMail;
        this.groupOrderService = groupOrderService;
        this.itemService = itemService;
    }

    public MimeMessagePreparator constructOrderEmail(GroupOrderDTO order, boolean isHtml) {
        InternetAddress address = new InternetAddress();
        address.setAddress(order.getCreator() + "@munchies.com");
        return mimeMessage -> {
            MimeMessageHelper email = new MimeMessageHelper(mimeMessage);
            email.setTo(order.getRestaurant().getEmail());
            email.setSubject("Order Confirmation - " + order.getId());
            email.setText(order.getCreator()+" \n "+order.getTimestamp()+" \n "+itemService.findAllByOrderId(order.getId()).toString(),isHtml);
            email.setFrom(address);
        };
    }

    @Async()
    public void sendMail(MimeMessagePreparator mimeMessagePreparator, int id) {
        GroupOrderDTO groupOrderDTO = groupOrderService.findById(id);
        if (groupOrderService.timer(id) <= 0 && !groupOrderDTO.isSent() && itemService.findAllByOrderId(groupOrderDTO.getId()).size() > 0) {
            sendMail.send(mimeMessagePreparator);
            groupOrderDTO.setSent(true);
            groupOrderService.saveOrUpdate(groupOrderDTO);
        }
    }
}

