package com.munchies.api.services.mail.task;

import com.munchies.api.services.data.GroupOrderService;
import com.munchies.api.services.data.ItemService;
import com.munchies.api.services.mail.MailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;



@Component()
@Slf4j
public class MyTask implements Runnable {

    private GroupOrderService groupOrderService;

    private MailService mailService;

    private ItemService itemService;

    public MyTask(GroupOrderService groupOrderService, MailService mailService, ItemService itemService) {
        this.groupOrderService = groupOrderService;
        this.mailService = mailService;
        this.itemService = itemService;
    }

    @Override
    public void run() {
        groupOrderService.findAll().stream()
                .filter(groupOrderDTO -> (!itemService.findAllByOrderId(groupOrderDTO.getId()).isEmpty() || itemService.findAllByOrderId(groupOrderDTO.getId()) != null) && !groupOrderDTO.isSent())
                .peek(groupOrderDTO -> {
                    MimeMessagePreparator mimeMessagePreparator = mailService.constructOrderEmail(groupOrderDTO, false);
                    mailService.sendMail(mimeMessagePreparator, groupOrderDTO.getId());
                }).count();
    }
}
