package com.munchies.api.services.paypal.enums;

public enum Currency {
    EUR,USD,RSD,CHF
}
