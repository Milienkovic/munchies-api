package com.munchies.api.services.paypal;

import com.munchies.api.models.payload.UserDTO;
import com.munchies.api.services.data.UserService;
import com.munchies.api.services.paypal.enums.PaypalPaymentIntent;
import com.munchies.api.services.paypal.utils.PayPalUtils;
import com.paypal.api.payments.Payment;
import com.paypal.api.payments.PaymentExecution;
import com.paypal.api.payments.RedirectUrls;
import com.paypal.api.payments.Transaction;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PaymentService {

    private APIContext context;

    private TransactionService transactionService;

    private UserService userService;

    public PaymentService(APIContext context, TransactionService transactionService, UserService userService) {
        this.context = context;
        this.transactionService = transactionService;
        this.userService = userService;
    }

    public Payment createPayment(Integer orderId, String userEmail, String successUrl, String cancelUrl) throws PayPalRESTException {
        Payment payment = new Payment();
        payment.setRedirectUrls(redirectUrls(successUrl,cancelUrl));
        Transaction transaction = transactionService.createTransaction(orderId);
        List<Transaction> transactions = new ArrayList<>();
        transactions.add(transaction);
        payment.setTransactions(transactions);
        UserDTO user= userService.findByEmail(userEmail);
        payment.setPayer(PayPalUtils.createPayer(user));
        payment.setIntent(PaypalPaymentIntent.sale.toString());

        return payment.create(context);
    }

    private RedirectUrls redirectUrls(String successUrl, String cancelUrl){
        RedirectUrls redirectUrls = new RedirectUrls();
        redirectUrls.setReturnUrl(successUrl);
        redirectUrls.setCancelUrl(cancelUrl);
        return redirectUrls;
    }

    public Payment executePayment(String paymentId, String payerId) throws PayPalRESTException {
        Payment payment = new Payment();
        payment.setId(paymentId);
        PaymentExecution paymentExecution = new PaymentExecution();
        paymentExecution.setPayerId(payerId);
        return payment.execute(context, paymentExecution);
    }
}
