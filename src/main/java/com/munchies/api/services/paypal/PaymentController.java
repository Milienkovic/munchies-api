package com.munchies.api.services.paypal;


import com.paypal.api.payments.Links;
import com.paypal.api.payments.Payment;
import com.paypal.base.rest.PayPalRESTException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;

@RestController
@Slf4j
@RequestMapping("/api/v1")
public class PaymentController {

    private PaymentService paymentService;

    public PaymentController(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @PostMapping("/orders/{orderId}/pay")
    public ResponseEntity<String> payment(@PathVariable Integer orderId, HttpServletRequest request) throws PayPalRESTException {

        String successUrl = ServletUriComponentsBuilder
                .fromContextPath(request)
                .path("/api/v1/pay/success").toUriString();

        String cancelUrl = ServletUriComponentsBuilder.
                fromContextPath(request).path("/api/v1/pay/cancel").toUriString();

        Payment payment = paymentService
                .createPayment(orderId, request.getUserPrincipal().getName(), successUrl, cancelUrl);

        for (Links link :
                payment.getLinks()) {
            if (link.getRel().equals("approval_url")) {
                HttpHeaders headers = new HttpHeaders();
                headers.setLocation(URI.create(link.getHref()));
                return ResponseEntity
                        .ok()
                        .headers(headers)
                        .body(link.getHref());
            }
        }
        return ResponseEntity.badRequest().build();
    }


    @GetMapping("/pay/cancel")
    public ResponseEntity<String> cancelPay(HttpServletRequest request) {
        return ResponseEntity.ok(ServletUriComponentsBuilder.
                fromContextPath(request).path("/pay/cancel").toUriString());
    }

    @GetMapping("/pay/success")
    public ResponseEntity<?> successPay(@RequestParam("paymentId") String paymentId, @RequestParam("PayerID") String payerId) {
        try {
            Payment payment = paymentService.executePayment(paymentId, payerId);
            log.error("payment: " + payment);

            if (payment.getState().equals("approved")) {
                return ResponseEntity.ok(payment.getState());
            }
        } catch (PayPalRESTException e) {
            log.error(e.getMessage());
        }
        return ResponseEntity.unprocessableEntity().build();
    }
}
