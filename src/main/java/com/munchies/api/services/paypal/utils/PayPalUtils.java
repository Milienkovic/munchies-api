package com.munchies.api.services.paypal.utils;

import com.munchies.api.models.payload.UserDTO;
import com.munchies.api.services.paypal.enums.PaypalPaymentMethod;
import com.paypal.api.payments.Amount;
import com.paypal.api.payments.Payer;
import com.paypal.api.payments.PayerInfo;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class PayPalUtils {

    public static Payer createPayer(UserDTO user) {
        Payer payer = new Payer();
        payer.setPayerInfo(getPayerInfo(user));
        payer.setPaymentMethod(PaypalPaymentMethod.paypal.toString());
        return payer;

    }

    private static PayerInfo getPayerInfo(UserDTO user) {
        PayerInfo payerInfo = new PayerInfo();
        payerInfo.setFirstName(user.getFirstName());
        payerInfo.setLastName(user.getLastName());
        payerInfo.setEmail(user.getEmail());
        return payerInfo;
    }

    public static Amount createAmount(Double total, String currency){
        Amount amount = new Amount();
        amount.setCurrency(currency);
        double totalAmount = new BigDecimal(total).setScale(2, RoundingMode.HALF_UP).doubleValue();
        String totalString = String.format("%.2f",totalAmount);
        amount.setTotal(totalString);
        return amount;
    }
}
