package com.munchies.api.services.paypal.enums;

public enum PaypalPaymentIntent {
    sale, authorize, order
}
