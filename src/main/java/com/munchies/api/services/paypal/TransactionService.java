package com.munchies.api.services.paypal;

import com.munchies.api.models.payload.GroupOrderDTO;
import com.munchies.api.services.data.GroupOrderService;
import com.munchies.api.services.data.ItemService;
import com.munchies.api.services.paypal.enums.Currency;
import com.munchies.api.services.paypal.utils.PayPalUtils;
import com.paypal.api.payments.Item;
import com.paypal.api.payments.ItemList;
import com.paypal.api.payments.Payee;
import com.paypal.api.payments.Transaction;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class TransactionService {


    private GroupOrderService groupOrderService;

    private ItemService itemService;

    public TransactionService(GroupOrderService groupOrderService, ItemService itemService) {
        this.groupOrderService = groupOrderService;
        this.itemService = itemService;
    }

    public Transaction createTransaction(Integer orderId) {
        GroupOrderDTO groupOrderDTO = groupOrderService.findById(orderId);
        Transaction transaction = new Transaction();
        transaction.setAmount(PayPalUtils.createAmount(itemService.totalSum(orderId), Currency.USD.toString()));
        transaction.setDescription("Order No:" + orderId + " for restaurant: " + groupOrderDTO.getRestaurant().getName());
        transaction.setItemList(getItems(orderId));

        Payee payee = new Payee();
        payee.setEmail(groupOrderDTO.getRestaurant().getEmail());
        return transaction;
    }

    private ItemList getItems(Integer orderId) {
        ItemList itemList = new ItemList();
        return itemList.setItems(
                itemService.findAllByOrderId(orderId).stream()
                        .map(itemDTO -> {
                            Item payPalItem = new Item();
                            payPalItem.setName(itemDTO.getItemName());
                            payPalItem.setPrice(itemDTO.getPrice());
                            payPalItem.setDescription("Meal");
                            payPalItem.setCurrency(Currency.USD.toString());
                            payPalItem.setQuantity(itemDTO.getQuantity()+"");
                            return payPalItem;
                        }).collect(Collectors.toList()));
    }
}
