package com.munchies.api.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class StorageProperties {
    @Value("${file.upload-dir}")
//    private String location = "C:\\Users\\mix\\IdeaProjects\\munchies\\src\\main\\resources\\savedFiles";
    private String location;

    public String getLocation() {
        return location;

    }
}
