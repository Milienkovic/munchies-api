package com.munchies.api.repo;

import com.munchies.api.models.entities.GroupOrder;
import com.munchies.api.models.entities.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupOrderRepo extends JpaRepository<GroupOrder,Integer> {
    List<GroupOrder> findAllByRestaurantId(Integer id);


}
