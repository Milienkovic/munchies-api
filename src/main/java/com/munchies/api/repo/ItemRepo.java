package com.munchies.api.repo;


import com.munchies.api.models.entities.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemRepo extends JpaRepository<Item,Integer> {
    List<Item> findAllByGroupOrderId(Integer id);
}
