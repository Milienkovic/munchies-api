package com.munchies.api.repo;


import com.munchies.api.models.entities.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RestaurantRepo extends JpaRepository<Restaurant,Integer> {

    Optional<Restaurant> findById(Integer integer);
    Optional<Restaurant> findByName(String name);
    Optional<Restaurant> findByPhone(String phone);
    Optional<Restaurant> findByEmail(String email);
    Boolean existsByPhone(String phone);
    Boolean existsByEmail(String email);

}
