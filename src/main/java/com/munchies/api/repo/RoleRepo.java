package com.munchies.api.repo;

import com.munchies.api.models.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepo extends JpaRepository<Role, Integer> {
    Optional<Role> findById(Integer integer);

    Optional<Role> findByRole(String role);
}
