package com.munchies.api;

import com.munchies.api.services.data.StorageService;
import com.munchies.api.config.PaypalProps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableJpaAuditing
@EnableScheduling
@EnableConfigurationProperties(PaypalProps.class)
@SpringBootApplication
public class ApiApplication implements CommandLineRunner {

    @Autowired
    StorageService storageService;


    public static void main(String[] args) {
        SpringApplication.run(ApiApplication.class, args);
    }

    @Override
    public void run(String... args){
        storageService.init();
    }
}
