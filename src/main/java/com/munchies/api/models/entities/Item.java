package com.munchies.api.models.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "item_name")
    private String itemName;

    @Column(name = "creator")
    private String creator;

    @Column(name = "price")
    private double price;

    @ManyToOne
    @JoinColumn(name = "group_order_id", referencedColumnName = "id")
    private GroupOrder groupOrder;

    @Column
    private int quantity;
}
