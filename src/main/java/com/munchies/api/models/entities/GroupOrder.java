package com.munchies.api.models.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "group_order", schema = "munchies")
@EntityListeners(AuditingEntityListener.class)

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GroupOrder implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "timeout")
    private String timeout;

    @Column(name = "creator")
    private String creator;

    @ManyToOne
    @JoinColumn(name = "restaurant_id")
    private Restaurant restaurant;

    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    @Column(name = "tstamp", nullable = false, updatable = false)
    private Date timestamp;

    @Column(name = "auto_send")
    private boolean autoSend;
    @Column(name = "sent")
    private boolean sent;

    @OneToMany(mappedBy = "groupOrder", cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<Item> items;

}
