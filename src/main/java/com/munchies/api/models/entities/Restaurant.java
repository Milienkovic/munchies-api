package com.munchies.api.models.entities;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder

@DynamicUpdate
public class Restaurant {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "address")
    private String address;

    @Column(name = "phone")
    private String phone;

    @Column(name = "menu_url")
    private String menuUrl;

    @Column(name = "delivery_info")
    private String deliveryInfo;

    @Column(name = "additional_charges")
    private String additionalCharges;

    @Column(name = "email")
    private String email;

    @OneToMany(mappedBy = "restaurant", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<GroupOrder> groupOrders;

    @Column(name = "account")
    private String accountNumber;

}
