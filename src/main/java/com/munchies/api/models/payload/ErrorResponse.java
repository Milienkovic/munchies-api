package com.munchies.api.models.payload;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class ErrorResponse {
    private String message;
    private int status;
    private Timestamp timestamp;

}
