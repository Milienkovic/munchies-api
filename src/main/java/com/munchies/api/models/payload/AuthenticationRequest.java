package com.munchies.api.models.payload;

import com.munchies.api.services.validators.Email;
import com.munchies.api.services.validators.Password;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AuthenticationRequest implements Serializable {
    @Email
    private String username;
    @Password
    private String password;
}
