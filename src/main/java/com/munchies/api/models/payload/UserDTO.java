package com.munchies.api.models.payload;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.munchies.api.models.entities.Role;
import com.munchies.api.services.validators.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Set;

@Data
public class UserDTO {

    private int id;

    @PersonName(message = "Invalid First Name")
    private String firstName;

    @PersonName(message = "Invalid Last Name")
    private String lastName;

    @Phone
    private String phone;

    @Email
    private String email;

    @Password
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @Address
    private String address;

    @EqualsAndHashCode.Exclude
    private Set<Role> userRoles;

}
