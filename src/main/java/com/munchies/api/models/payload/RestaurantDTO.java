package com.munchies.api.models.payload;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.munchies.api.services.markers.Views;
import com.munchies.api.services.validators.Address;
import com.munchies.api.services.validators.Email;
import com.munchies.api.services.validators.Name;
import com.munchies.api.services.validators.Phone;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class RestaurantDTO {

    @JsonView(Views.Public.class)
    private int id;

    @JsonView(Views.Public.class)
    @Name
    private String name;

    @JsonView(Views.Public.class)
    @Address
    private String address;

    @JsonView(Views.Public.class)
    @Phone
    private String phone;

    @JsonView(Views.Public.class)
    private String menuUrl;

    @JsonView(Views.Public.class)
    private String deliveryInfo;

    @JsonView(Views.Public.class)
    @Size(min = 2, max = 250)
    private String additionalCharges;

    @JsonView(Views.Public.class)
    @Email
    private String email;


    private String accountNumber;
}
