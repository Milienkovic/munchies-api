package com.munchies.api.models.payload;


import com.fasterxml.jackson.annotation.*;
import com.munchies.api.services.markers.Views;
import com.munchies.api.services.validators.Name;
import com.munchies.api.services.validators.PersonName;
import com.munchies.api.services.validators.Price;
import lombok.*;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString(exclude = {"groupOrder", "id"})
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class ItemDTO {

    @JsonView(Views.Public.class)
    private int id;

    @JsonView(Views.Public.class)
    @Name
    private String itemName;

    @JsonView(Views.Public.class)
    @PersonName
    private String creator;

    @JsonView(Views.Public.class)
    @Price
    private String price;

    @JsonView(Views.Internal.class)
    private GroupOrderDTO groupOrder;

    @Enumerated(EnumType.STRING)
    private String currency;

    private int quantity;

}
