package com.munchies.api.models.payload;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.munchies.api.services.markers.Views;
import com.munchies.api.services.validators.PersonName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder

@JsonIgnoreProperties(value = "timestamp", allowGetters = true)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class GroupOrderDTO {

    @JsonView(Views.Public.class)
    private int id;

    @JsonView(Views.Public.class)
    private String timeout;

    @JsonView(Views.Public.class)
    @PersonName
    private String creator;

    @JsonView(Views.Internal.class)
    private RestaurantDTO restaurant;

    @JsonView(Views.Public.class)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date timestamp;

    @JsonView(Views.Public.class)
    private boolean autoSend;

    @JsonView(Views.Public.class)
    private boolean sent;

}
